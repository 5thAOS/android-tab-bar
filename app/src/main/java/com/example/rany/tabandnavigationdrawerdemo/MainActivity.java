package com.example.rany.tabandnavigationdrawerdemo;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.rany.tabandnavigationdrawerdemo.adapter.MyPagerAdapter;
import com.example.rany.tabandnavigationdrawerdemo.fragments.FavoriteFragment;
import com.example.rany.tabandnavigationdrawerdemo.fragments.HomeFragment;
import com.example.rany.tabandnavigationdrawerdemo.fragments.NotificationFragment;
import com.example.rany.tabandnavigationdrawerdemo.model.TabModel;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.tabView);
        viewPager = findViewById(R.id.vpView);

        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        adapter.addTab(new TabModel("Home",
                R.drawable.home,
                new HomeFragment()));
        adapter.addTab(new TabModel("Favorite",
                R.drawable.favorite,
                new FavoriteFragment()));
        adapter.addTab(new TabModel("Notification",
                R.drawable.notification,
                new NotificationFragment()));


        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        adapter.setTabIcon(tabLayout);

    }
}
