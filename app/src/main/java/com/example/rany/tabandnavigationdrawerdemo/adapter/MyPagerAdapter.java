package com.example.rany.tabandnavigationdrawerdemo.adapter;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.rany.tabandnavigationdrawerdemo.model.TabModel;

import java.util.ArrayList;
import java.util.List;

public class MyPagerAdapter extends FragmentPagerAdapter{

    private List<TabModel> tabModelList;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
        tabModelList = new ArrayList<>();
    }

    public void addTab(TabModel tabModel){
        tabModelList.add(tabModel);
    }

    @Override
    public Fragment getItem(int position) {
        return tabModelList.get(position).getFragment();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
//        return tabModelList.get(position).getName();
        return null;
    }

    public void setTabIcon(TabLayout tabLayout){
        for (int i = 0; i < tabLayout.getTabCount(); i++){
            tabLayout.getTabAt(i).setIcon(tabModelList.get(i).getIcon());
        }
    }

    @Override
    public int getCount() {
        return tabModelList.size();
    }


}
