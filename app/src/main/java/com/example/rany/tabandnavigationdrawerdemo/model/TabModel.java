package com.example.rany.tabandnavigationdrawerdemo.model;

import android.support.v4.app.Fragment;

public class TabModel {

    private String name;
    private int icon;
    private Fragment fragment;

    public TabModel(String name, int icon, Fragment fragment) {
        this.name = name;
        this.icon = icon;
        this.fragment = fragment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }
}
